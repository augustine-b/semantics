package typechecker;

import expression.DeclarationNode;
import expression.ExpressionNode;
import expression.IdentifierNode;
import expression.NodeType;
import expression.ProcedureDeclarationNode;
import org.antlr.v4.runtime.misc.Pair;
import typechecker.type.AnnotatedType;
import typechecker.type.PrimitiveType;
import typechecker.type.Type;

public class TypeCheckerFactory {

	public TypeChecker buildTypeChecker() {
		TypeChecker typeChecker = new TypeChecker();

		typeChecker.addCheck(NodeType.Root)
				.withParameters(((context, node, types) -> {
					for (ExpressionNode child : node.getChildren()) {
						if (!(child instanceof ProcedureDeclarationNode)
								&& !(child instanceof DeclarationNode)) {
							return false;
						}
					}

					return true;
				})).resultsIn(PrimitiveType.Void);

		typeChecker.addCheck(NodeType.Declaration)
				.withParameters()
				.withSideEffect((context, node, types) -> {
					DeclarationNode declarationNode = (DeclarationNode) node;

					if (declarationNode.getType().getCategory() == ValueCategory.LValue) {
						throw new UnsupportedOperationException("Variables with reference " +
								"types are only supported in procedure headers.");
					}

					int stackPosition = context.addVariable(declarationNode.getName(),
							declarationNode.getType().getType());
					declarationNode.setStackPosition(stackPosition);
				}).resultsIn(((context, node, types) -> {
					DeclarationNode declarationNode = (DeclarationNode) node;
					return declarationNode.getType().asLValue();
				}));

		typeChecker.addCheck(NodeType.Assignment)
				.withParameters((context, node, types) ->
					types.length == 2
						&& types[0].getCategory() == ValueCategory.LValue
						&& types[0].getType().supertypeOf(types[1].getType())
				).resultsIn((context, node, types) -> types[0].asRValue());

		typeChecker.addCheck(NodeType.Identifier)
				.withParameters()
				.resultsIn((context, node, types) -> {
					IdentifierNode identifierNode = (IdentifierNode) node;
					Pair<Type, Integer> variable = context.lookupVariable(identifierNode.getName());
					identifierNode.setStackPosition(variable.b);
					return new AnnotatedType(variable.a, ValueCategory.LValue);
				});

		typeChecker.setHasFrame(NodeType.If);

		typeChecker.addCheck(NodeType.If)
				.withParameters((context, node, types) ->
					types.length == 2 && types[0].getType() == PrimitiveType.Boolean
				).resultsIn((context, node, types) -> types[1].asRValue());

		typeChecker.addCheck(NodeType.If)
				.withParameters((context, node, types) ->
					types.length == 3 && types[0].getType() == PrimitiveType.Boolean
				).resultsIn((context, node, types) -> {
					if (types[1].getType() == types[2].getType()) {
						if (types[1].getCategory() == types[2].getCategory()) {
							return types[1];
						} else {
							return types[1].asRValue();
						}
					}
					return new AnnotatedType(PrimitiveType.Void, ValueCategory.RValue);
				});

		typeChecker.setHasFrame(NodeType.While);
		typeChecker.addCheck(NodeType.While)
				.withParameters((context, node, types) ->
					types.length == 2 && types[0].getType() == PrimitiveType.Boolean
				).resultsIn((context, node, types) -> types[1].asRValue());

		typeChecker.setHasFrame(NodeType.Block);
		typeChecker.addCheck(NodeType.Block)
				.withParameters((context, node, types) -> true)
				.resultsIn((context, node, types) ->
					types.length > 0
						? types[types.length - 1]
						: new AnnotatedType(PrimitiveType.Void, ValueCategory.RValue)
				);

		typeChecker.addCheck(NodeType.Integer)
				.withParameters()
				.resultsIn(PrimitiveType.Integer);

		typeChecker.addCheck(NodeType.Boolean)
				.withParameters()
				.resultsIn(PrimitiveType.Boolean);

		addIntegerOperation(typeChecker, NodeType.Plus);
		addIntegerOperation(typeChecker, NodeType.Minus);
		addIntegerOperation(typeChecker, NodeType.Multiply);
		addIntegerOperation(typeChecker, NodeType.Divide);
		addIntegerOperation(typeChecker, NodeType.Modulo);

		typeChecker.addCheck(NodeType.Minus)
				.withParameters(PrimitiveType.Integer)
				.resultsIn(PrimitiveType.Integer);

		addBooleanOperation(typeChecker, NodeType.Equals);
		addBooleanOperation(typeChecker, NodeType.NotEquals);
		addBooleanOperation(typeChecker, NodeType.And);
		addBooleanOperation(typeChecker, NodeType.Or);

		typeChecker.addCheck(NodeType.Not)
				.withParameters(PrimitiveType.Boolean)
				.resultsIn(PrimitiveType.Boolean);

		addComparisonOperation(typeChecker, NodeType.Equals);
		addComparisonOperation(typeChecker, NodeType.NotEquals);
		addComparisonOperation(typeChecker, NodeType.Less);
		addComparisonOperation(typeChecker, NodeType.LessEquals);
		addComparisonOperation(typeChecker, NodeType.Greater);
		addComparisonOperation(typeChecker, NodeType.GreaterEquals);

		return typeChecker;
	}

	private void addComparisonOperation(TypeChecker typeChecker, NodeType nodeType) {
		typeChecker.addCheck(nodeType)
				.withParameters(PrimitiveType.Integer, PrimitiveType.Integer)
				.resultsIn(PrimitiveType.Boolean);
	}

	private void addIntegerOperation(TypeChecker typeChecker, NodeType nodeType) {
		typeChecker.addCheck(nodeType)
				.withParameters(PrimitiveType.Integer, PrimitiveType.Integer)
				.resultsIn(PrimitiveType.Integer);
	}

	private void addBooleanOperation(TypeChecker typeChecker, NodeType nodeType) {
		typeChecker.addCheck(nodeType)
				.withParameters(PrimitiveType.Boolean, PrimitiveType.Boolean)
				.resultsIn(PrimitiveType.Boolean);
	}
}
