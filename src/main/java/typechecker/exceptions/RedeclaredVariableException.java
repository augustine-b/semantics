package typechecker.exceptions;

public class RedeclaredVariableException extends RuntimeException {
	public RedeclaredVariableException(String name) {
		super("The variable " + name + " was redeclared in the same context");
	}
}
