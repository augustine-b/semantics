package typechecker;

import expression.ExpressionNode;
import typechecker.type.AnnotatedType;
import typechecker.type.PrimitiveType;

public class TypeCheckerRule {

	private PrimitiveType[] parameters;
	private ParameterCheckFn parameterChecker;

	private PrimitiveType resultType;
	private ResultTypeFn resultTypeComputer;

	private SideEffectFn sideEffect;

	AnnotatedType matches(TypeCheckingContext context, ExpressionNode expression, AnnotatedType[] childTypes) {
		if (parameters != null) {
			if (childTypes.length != parameters.length) {
				return null;
			}
			for (int i = 0; i < childTypes.length; i++) {
				if (childTypes[i].getType() != parameters[i]) {
					return null;
				}
			}
		} else if (parameterChecker != null) {
			if (!parameterChecker.checkParameters(context, expression, childTypes)) {
				return null;
			}
		} else {
			throw new Error("Malformed TypeCheckerRule: no parameter check set");
		}

		if (resultType != null) {
			return new AnnotatedType(resultType, ValueCategory.RValue);
		} else if (resultTypeComputer != null) {
			return resultTypeComputer.computeResultType(context, expression, childTypes);
		} else {
			throw new Error("Malformed TypeCheckerRule: no result type set");
		}
	}

	void executeSideEffect(TypeCheckingContext context, ExpressionNode expression, AnnotatedType[] childTypes) {
		if (sideEffect != null) {
			sideEffect.executeSideEffect(context, expression, childTypes);
		}
	}

	TypeCheckerRule withParameters(PrimitiveType... parameters) {
		this.parameters = parameters;
		return this;
	}

	TypeCheckerRule withParameters(ParameterCheckFn parameterChecker) {
		this.parameterChecker = parameterChecker;
		return this;
	}

	TypeCheckerRule resultsIn(PrimitiveType resultType) {
		this.resultType = resultType;
		return this;
	}

	TypeCheckerRule resultsIn(ResultTypeFn resultTypeComputer) {
		this.resultTypeComputer = resultTypeComputer;
		return this;
	}

	TypeCheckerRule withSideEffect(SideEffectFn sideEffect) {
		this.sideEffect = sideEffect;
		return this;
	}

	interface ParameterCheckFn {
		boolean checkParameters(TypeCheckingContext context,
				ExpressionNode node, AnnotatedType[] types);
	}

	interface ResultTypeFn {
		AnnotatedType computeResultType(TypeCheckingContext context,
				ExpressionNode node, AnnotatedType[] types);
	}

	interface SideEffectFn {
		void executeSideEffect(TypeCheckingContext context,
				ExpressionNode node, AnnotatedType[] types);
	}
}
