package typechecker.exceptions;

import typechecker.type.AnnotatedType;

public class FunctionReturnTypeMismatchException extends RuntimeException {
	public FunctionReturnTypeMismatchException(AnnotatedType returnType, AnnotatedType bodyType) {
		super("Invalid return type in procedure. Expected: " + returnType.toPrettyString()
				+ ", actual: " + bodyType.toPrettyString());
	}
}
