package typechecker;

import org.antlr.v4.runtime.misc.Pair;
import typechecker.exceptions.RedeclaredVariableException;
import typechecker.type.CallableType;
import typechecker.type.Type;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;

public class ProcedureFrame {

	private final ArrayList<Variable> variables;
	private final Deque<Integer> internalFrames;

	ProcedureFrame() {
		variables = new ArrayList<>();
		internalFrames = new ArrayDeque<>();
		internalFrames.push(0);
	}

	int addVariable(String name, Type type) {
		for (int i = internalFrames.peek(); i < variables.size(); i++) {
			if (variables.get(i).name.equals(name)) {
				throw new RedeclaredVariableException(name);
			}
		}

		variables.add(new Variable(name, type));
		return variables.size() - 1;
	}

	Pair<Type, Integer> getVariable(String name) {
		for (int i = variables.size() - 1; i >= 0; i--) {
			if (variables.get(i).name.equals(name)) {
				return new Pair<>(variables.get(i).type, i);
			}
		}

		return null;
	}

	int addProcedure(String name, CallableType procedureType) {
		variables.add(new Variable("proc " + name, procedureType));
		return variables.size() - 1;
	}

	List<Pair<CallableType, Integer>> getProcedures(String name) {
		List<Pair<CallableType, Integer>> procedures = new ArrayList<>();
		for (int i = variables.size() - 1; i >= 0; i--) {
			if (variables.get(i).name.equals("proc " + name)) {
				procedures.add(new Pair<>((CallableType) variables.get(i).type, i));
			}

			if (internalFrames.contains(i) && !procedures.isEmpty()) {
				return procedures;
			}
		}

		return procedures;
	}

	void pushFrame() {
		internalFrames.push(variables.size());
	}

	void popFrame() {
		variables.subList(internalFrames.pop(), variables.size()).clear();
	}

	private static class Variable {
		private final String name;
		private final Type type;

		private Variable(String name, Type type) {
			this.name = name;
			this.type = type;
		}
	}
}
