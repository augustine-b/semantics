package typechecker.type;

import interpreter.value.Value;

import java.util.Arrays;
import java.util.Objects;

public class CallableType implements Type {

	private final AnnotatedType[] inputTypes;
	private final AnnotatedType outputType;

	public CallableType(AnnotatedType[] inputTypes, AnnotatedType outputType) {
		this.inputTypes = inputTypes;
		this.outputType = outputType;
	}

	public AnnotatedType[] getInputTypes() {
		return inputTypes;
	}

	public AnnotatedType getOutputType() {
		return outputType;
	}

	@Override
	public String toString() {
		return toPrettyString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		CallableType that = (CallableType) o;
		return Arrays.equals(inputTypes, that.inputTypes)
				&& Objects.equals(outputType, that.outputType);
	}

	@Override
	public String toPrettyString() {
		StringBuilder sb = new StringBuilder();
		sb.append("<");
		for (int i = 0; i < inputTypes.length; i++) {
			if (i != 0) {
				sb.append(", ");
			}
			sb.append(inputTypes[i].toPrettyString());
		}
		sb.append("> -> ").append(outputType.toPrettyString());
		return sb.toString();
	}

	public boolean callableWith(AnnotatedType[] childTypes) {
		if (childTypes.length != inputTypes.length) {
			return false;
		}
		for (int i = 0; i < childTypes.length; i++) {
			if (!inputTypes[i].supertypeOf(childTypes[i])) {
				return false;
			}
		}

		return true;
	}

	@Override
	public boolean supertypeOf(Type other) {
		if (!(other instanceof CallableType)) {
			return false;
		}

		CallableType callable = (CallableType) other;
		if (callable.inputTypes.length != inputTypes.length) {
			return false;
		}

		for (int i = 0; i < callable.inputTypes.length; i++) {
			if (!callable.inputTypes[i].supertypeOf(inputTypes[i])) {
				return false;
			}
		}

		return outputType.supertypeOf(callable.outputType);
	}

	@Override
	public Value defaultValue() {
		return null;
	}
}
