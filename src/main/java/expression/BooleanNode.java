package expression;

import interpreter.value.BooleanValue;
import interpreter.value.Value;

public class BooleanNode extends ExpressionNode {

	private final boolean value;

	public BooleanNode(boolean value) {
		this.value = value;
	}

	@Override
	public String toPrettyString() {
		return String.valueOf(value);
	}

	@Override
	public NodeType getNodeType() {
		return NodeType.Boolean;
	}

	@Override
	public ExpressionNode[] getChildren() {
		return new ExpressionNode[0];
	}

	public Value getValue() {
		return new BooleanValue(value);
	}
}
