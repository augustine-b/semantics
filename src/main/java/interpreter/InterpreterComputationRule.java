package interpreter;

import expression.ExpressionNode;
import interpreter.value.Value;
import typechecker.type.PrimitiveType;

import java.util.Arrays;

public class InterpreterComputationRule {

	private PrimitiveType[] parameters;
	private ValueComputerFn computer;

	public InterpreterComputationRule withParameters(PrimitiveType... parameters) {
		this.parameters = parameters;
		return this;
	}

	public InterpreterComputationRule withComputer(ValueComputerFn computer) {
		this.computer = computer;
		return this;
	}

	interface ValueComputerFn {
		Value computeValue(ExpressionNode node, Value[] children);
	}

	public boolean matches(ExpressionNode expr) {
		return parameters == null || Arrays.equals(Arrays.stream(expr.getChildren())
				.map(node -> node.getComputedType().getType()).toArray(), parameters);
	}

	public Value compute(ExpressionNode expr, Value[] children) {
		return computer.computeValue(expr, children);
	}
}
