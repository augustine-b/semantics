package typechecker;

import org.antlr.v4.runtime.misc.Pair;
import typechecker.exceptions.AmbiguousReferenceException;
import typechecker.exceptions.UndeclaredVariableException;
import typechecker.type.CallableType;
import typechecker.type.PrimitiveType;
import typechecker.type.Type;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.List;
import java.util.stream.Collectors;

public class TypeCheckingContext {

	private final ProcedureFrame globalFrame;
	private final Deque<ProcedureFrame> frameStack;

	public TypeCheckingContext() {
		globalFrame = new ProcedureFrame();
		frameStack = new ArrayDeque<>();
		// to avoid confusion with negative zero
		globalFrame.addVariable("<<dummy>>", PrimitiveType.Void);
	}

	public void pushProcedureFrame() {
		frameStack.push(new ProcedureFrame());
	}

	void pushSimpleFrame() {
		assert frameStack.peek() != null;
		frameStack.peek().pushFrame();
	}

	public void popProcedureFrame() {
		frameStack.pop();
	}

	void popSimpleFrame() {
		assert frameStack.peek() != null;
		frameStack.peek().popFrame();
	}

	int addVariable(String name, Type type) {
		if (frameStack.peek() != null) {
			return frameStack.peek().addVariable(name, type);
		} else {
			return -globalFrame.addVariable(name, type);
		}
	}

	int addProcedure(String name, CallableType type) {
		if (frameStack.peek() != null) {
			return frameStack.peek().addProcedure(name, type);
		} else {
			return -globalFrame.addProcedure(name, type);
		}
	}

	Pair<Type, Integer> lookupVariable(String name) {
		if (frameStack.peek() != null) {
			Pair<Type, Integer> variable = getTypeFromFrame(frameStack.peek(), name);
			if (variable != null) {
				return variable;
			}
		}

		Pair<Type, Integer> variable = getTypeFromFrame(globalFrame, name);
		if (variable != null) {
			return new Pair<>(variable.a, -variable.b);
		}

		throw new UndeclaredVariableException(name);
	}

	private Pair<Type, Integer> getTypeFromFrame(ProcedureFrame frame, String name) {
		Pair<Type, Integer> variable = frame.getVariable(name);
		if (variable != null) {
			return variable;
		}

		List<Pair<CallableType, Integer>> procedures = frame.getProcedures(name);
		if (procedures.size() == 1) {
			return new Pair<>(procedures.get(0).a, procedures.get(0).b);
		} else if (procedures.size() > 1) {
			throw new AmbiguousReferenceException(name, procedures.stream()
					.map(p -> p.a).collect(Collectors.toList()));
		} else {
			return null;
		}
	}

	List<Pair<CallableType, Integer>> lookupProcedure(String name) {
		if (frameStack.peek() != null) {
			List<Pair<CallableType, Integer>> procedures = frameStack.peek().getProcedures(name);
			if (!procedures.isEmpty()) {
				return procedures;
			}
		}

		List<Pair<CallableType, Integer>> procedures = globalFrame.getProcedures(name);
		if (!procedures.isEmpty()) {
			return procedures.stream().map(p -> new Pair<>(p.a, -p.b)).collect(Collectors.toList());
		}

		throw new UndeclaredVariableException(name);
	}
}
