grammar Expressions;

@parser::header {
	import expression.*;
	import typechecker.*;
	import typechecker.type.*;
}

@parser::members {
	private ExpressionNode[] convert(List<ExprContext> values) {
		return values.stream().map(context -> context.value).toArray(ExpressionNode[]::new);
	}

	private DeclarationNode[] convertVariableDeclarations(List<VariableDeclContext> values) {
		return values.stream().map(context -> context.value).toArray(DeclarationNode[]::new);
	}

	private ExpressionNode[] convertDeclarations(List<DeclarationContext> values) {
		return values.stream().map(context -> context.value).toArray(ExpressionNode[]::new);
	}

	private AnnotatedType[] convertTypes(List<TypeContext> values) {
		return values.stream().map(context -> context.value).toArray(AnnotatedType[]::new);
	}
}

root returns[ExpressionNode value]
	: (decls+=declaration)+										{ $value = new OperatorNode(NodeType.Root, convertDeclarations($decls)); }
;

declaration returns[ExpressionNode value]
	: vard=variableDecl	';'										{ $value = $vard.value; }
	| procd=procedureDecl										{ $value = $procd.value; }
;

type returns[AnnotatedType value]
	: primitiveType=Type										{ $value = new AnnotatedType(PrimitiveType.lookup($primitiveType.getText())); }
	| 'ref' primitiveType=Type									{ $value = new AnnotatedType(PrimitiveType.lookup($primitiveType.getText()), ValueCategory.LValue); }
	| '<' (param+=type)? (',' param+=type)* '>' '->' ret=type	{ $value = new AnnotatedType(new CallableType(convertTypes($param), $ret.value)); }
;

procedureDecl returns[ExpressionNode value]	@init{ boolean hasRet = false; }
	: 'proc' (name=String)? '(' (types+=variableDecl ',')* (types+=variableDecl)? ')' ('->' ret=type {hasRet = true; })? body=expr	{
		$value = new ProcedureDeclarationNode($name == null ? null : $name.getText(), convertVariableDeclarations($types),
			hasRet ? $ret.value : null, $body.value);
};

variableDecl returns[ExpressionNode value]
	: id=String ':' tp=type										{ $value = new DeclarationNode($id.getText(), $tp.value); }
;

expr returns[ExpressionNode value]
	: 'proc' (name=String)? '(' (types+=variableDecl ',')* (types+=variableDecl)? ')' ('->' ret=Type)? body=expr	{
      		$value = new ProcedureDeclarationNode($name == null ? null : $name.getText(), convertVariableDeclarations($types),
      			$ret == null ? null : new AnnotatedType(PrimitiveType.lookup($ret.getText())), $body.value);
      }
	| func=expr '(' (param+=expr)? (',' param+=expr)* ')'	{ $value = new ProcedureCallNode($func.value, convert($param)); }
	| var=variableDecl										{ $value = $var.value; }
	| 'if' cond=expr 'then' thenb=expr 'else' elseb=expr	{ $value = new OperatorNode(NodeType.If, $cond.value, $thenb.value, $elseb.value); }
	| 'if' cond=expr 'then' thenb=expr						{ $value = new OperatorNode(NodeType.If, $cond.value, $thenb.value); }
	| 'while' cond=expr 'do' dob=expr						{ $value = new OperatorNode(NodeType.While, $cond.value, $dob.value); }
	| '{' (vals+=expr ';')* '}'								{ $value = new OperatorNode(NodeType.Block, convert($vals)); }
	| op='-' rhs=expr										{ $value = new OperatorNode(NodeType.lookup($op.getText()), $rhs.value); }
	| lhs=expr op=('/' | '*' | '%') rhs=expr				{ $value = new OperatorNode(NodeType.lookup($op.getText()), $lhs.value, $rhs.value); }
	| lhs=expr op=('+' | '-') rhs=expr						{ $value = new OperatorNode(NodeType.lookup($op.getText()), $lhs.value, $rhs.value); }
	| lhs=expr op=('<' | '<=' | '>' | '>=') rhs=expr		{ $value = new OperatorNode(NodeType.lookup($op.getText()), $lhs.value, $rhs.value); }
	| lhs=expr op=('==' | '!=') rhs=expr					{ $value = new OperatorNode(NodeType.lookup($op.getText()), $lhs.value, $rhs.value); }
	| op='!' rhs=expr										{ $value = new OperatorNode(NodeType.lookup($op.getText()), $rhs.value); }
	| lhs=expr op='&&' rhs=expr								{ $value = new OperatorNode(NodeType.lookup($op.getText()), $lhs.value, $rhs.value); }
	| lhs=expr op='||' rhs=expr								{ $value = new OperatorNode(NodeType.lookup($op.getText()), $lhs.value, $rhs.value); }
	| lhs=expr op='=' rhs=expr								{ $value = new OperatorNode(NodeType.lookup($op.getText()), $lhs.value, $rhs.value); }
	| '(' val=expr ')'										{ $value = $val.value; }
	| id=String												{ $value = new IdentifierNode($id.getText()); }
	| bool=Boolean											{ $value = new BooleanNode("true".equals($bool.getText())); }
	| num=Number											{ $value = new IntegerNode(Integer.parseInt($num.getText())); }
;

Number: 		('0'..'9')+;
Boolean:		'true' | 'false';

Type:			'int' | 'bool' | 'void';
String:			('A'..'Z' | 'a'..'z') ('A'..'Z' | 'a'..'z' | '0'..'9')*;

Whitespace:		[ \r\n\t]+ -> skip;
