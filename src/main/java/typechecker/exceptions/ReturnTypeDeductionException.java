package typechecker.exceptions;

public class ReturnTypeDeductionException extends RuntimeException {
	public ReturnTypeDeductionException(String name) {
		super("Deducing the return type of the recursive function " + name
			+ " has failed. Please specify the return type.");
	}
}
