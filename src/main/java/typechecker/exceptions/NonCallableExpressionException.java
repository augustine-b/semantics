package typechecker.exceptions;

import expression.ExpressionNode;
import typechecker.type.Type;

public class NonCallableExpressionException extends RuntimeException {
	public NonCallableExpressionException(ExpressionNode expression, Type identifierType) {
		super("The expression " + expression.toPrettyString() + " specifies a variable of type "
				+ identifierType.toPrettyString() + " which is not a callable type");
	}
}
