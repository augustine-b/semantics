package interpreter.value;

import expression.ProcedureDeclarationNode;

public class ProcedureValue extends Value {

	private ProcedureDeclarationNode declarationNode;

	public ProcedureValue(ProcedureDeclarationNode declarationNode) {
		this.declarationNode = declarationNode;
	}

	@Override
	public void setValue(Value other) {
		declarationNode = other.getProcedureValue();
	}

	@Override
	public int getIntValue() {
		return 0;
	}

	@Override
	public boolean getBooleanValue() {
		return false;
	}

	@Override
	public ProcedureDeclarationNode getProcedureValue() {
		return declarationNode;
	}

	@Override
	public Value duplicate() {
		return new ProcedureValue(declarationNode);
	}
}
