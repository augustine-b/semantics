package interpreter.value;

import expression.ProcedureDeclarationNode;
import interpreter.exceptions.InvalidOperationError;

public class IntegerValue extends Value {

	private int value;

	public IntegerValue(int value) {
		this.value = value;
	}

	@Override
	public void setValue(Value other) {
		value = other.getIntValue();
	}

	@Override
	public int getIntValue() {
		return value;
	}

	@Override
	public boolean getBooleanValue() {
		throw new InvalidOperationError("Casting integer to boolean");
	}

	@Override
	public ProcedureDeclarationNode getProcedureValue() {
		return null;
	}

	@Override
	public Value duplicate() {
		return new IntegerValue(value);
	}
}
