package expression;

import typechecker.type.AnnotatedType;

public abstract class ExpressionNode {

	private AnnotatedType computedType;

	public abstract String toPrettyString();

	public abstract NodeType getNodeType();

	public abstract ExpressionNode[] getChildren();

	public AnnotatedType getComputedType() {
		return computedType;
	}

	public void setComputedType(AnnotatedType computedType) {
		this.computedType = computedType;
	}
}
