package typechecker;

import expression.ExpressionNode;
import org.antlr.v4.runtime.misc.Pair;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.opentest4j.AssertionFailedError;
import parser.ExpressionParser;
import typechecker.exceptions.FunctionReturnTypeMismatchException;
import typechecker.exceptions.InvalidParameterTypesException;
import typechecker.exceptions.NoMatchingOverloadException;
import typechecker.exceptions.NonCallableExpressionException;
import typechecker.exceptions.TypeError;
import typechecker.exceptions.UndeclaredVariableException;
import typechecker.type.AnnotatedType;
import typechecker.type.CallableType;
import typechecker.type.PrimitiveType;
import typechecker.type.Type;

import java.util.List;

class TypeCheckerTest {

	private static TypeChecker checker;

	@BeforeAll
	public static void setupChecker() {
		TypeCheckerFactory checkerFactory = new TypeCheckerFactory();
		checker = checkerFactory.buildTypeChecker();
	}

	@Test
	public void testSimpleExpressions() {
		t("3+5", PrimitiveType.Integer);
		t("true || false", PrimitiveType.Boolean);
		t("3*4+5 == 17 && 5 < 6", PrimitiveType.Boolean);
	}

	@Test
	public void testUndeclaredVariables() {
		undeclared("3 - a", "a");
		undeclared("if abba then abcd", "abba");
	}

	@Test
	public void testSimpleVariableDeclarations() {
		t("{a:int = 5; a + 3;}", PrimitiveType.Integer);
		t("{a:int = 4; b: int = 2 * 2; c: bool = a == b;}", PrimitiveType.Boolean);
	}

	@Test
	public void testBlocks() {
		t("{a: int = 3; {a = a + 2;};}", PrimitiveType.Integer);
		undeclared("{{a: int = 3;}; {a = a + 2;};}", "a");
		t("{a: int = 3; {a = a + 2;}; a = a + 3;}", PrimitiveType.Integer);
	}

	@Test
	public void testDeclaringProcedures() {
		t("proc alma() {}",
				new CallableType(new AnnotatedType[0], new AnnotatedType(PrimitiveType.Void)));
		t("proc foo(a: int) {}",
				new CallableType(new AnnotatedType[] {
						new AnnotatedType(PrimitiveType.Integer)
				}, new AnnotatedType(PrimitiveType.Void)));
		t("proc bar(a: bool, b: int) {}",
				new CallableType(new AnnotatedType[] {
						new AnnotatedType(PrimitiveType.Boolean),
						new AnnotatedType(PrimitiveType.Integer)
				}, new AnnotatedType(PrimitiveType.Void)));
		t("proc bar(a: bool, b: ref int) {}",
				new CallableType(new AnnotatedType[] {
						new AnnotatedType(PrimitiveType.Boolean),
						new AnnotatedType(PrimitiveType.Integer, ValueCategory.LValue)
				}, new AnnotatedType(PrimitiveType.Void)));
		t("proc bar(a: ref bool, b: int) {}",
				new CallableType(new AnnotatedType[] {
						new AnnotatedType(PrimitiveType.Boolean, ValueCategory.LValue),
						new AnnotatedType(PrimitiveType.Integer)
				}, new AnnotatedType(PrimitiveType.Void)));
		t("proc bar(a: ref bool, b: ref int) {}",
				new CallableType(new AnnotatedType[] {
						new AnnotatedType(PrimitiveType.Boolean, ValueCategory.LValue),
						new AnnotatedType(PrimitiveType.Integer, ValueCategory.LValue)
				}, new AnnotatedType(PrimitiveType.Void)));
		t("proc foo() -> int {4;}",
				new CallableType(new AnnotatedType[0], new AnnotatedType(PrimitiveType.Integer)));
		t("proc foo() -> void {4;}",
				new CallableType(new AnnotatedType[0], new AnnotatedType(PrimitiveType.Void)));

		Assertions.assertThrows(FunctionReturnTypeMismatchException.class, () -> {
			t("proc foo() -> bool {4;}", null);
		});
		Assertions.assertThrows(FunctionReturnTypeMismatchException.class, () -> {
			t("proc foo() -> int {}", null);
		});
		Assertions.assertThrows(FunctionReturnTypeMismatchException.class, () -> {
			t("proc foo() -> int {5 == 5;}", null);
		});
	}

	@Test
	public void testImmediatelyCalledProcedures() {
		t("proc(){}()", PrimitiveType.Void);
		t("proc(a: int){}(5)", PrimitiveType.Void);
		t("proc(a: int) -> void {a;}(5)", PrimitiveType.Void);
		t("proc(a: bool){a || false;}(true)", PrimitiveType.Boolean);
		t("proc(a: bool, b: int){x: bool = a || false; b + 3;}(true, 42)", PrimitiveType.Integer);
		t("proc alma(a: int){}(5)", PrimitiveType.Void);

		Assertions.assertThrows(NonCallableExpressionException.class, () -> {
			t("(3+1)(2)", null);
		});
		Assertions.assertThrows(InvalidParameterTypesException.class, () -> {
			t("proc(a: bool){}(5)", null);
		});
		Assertions.assertThrows(InvalidParameterTypesException.class, () -> {
			t("proc(a: ref bool){}(true)", null);
		});

		t("proc(a: ref bool){a;}(a: bool)", PrimitiveType.Boolean);

		Assertions.assertThrows(InvalidParameterTypesException.class, () -> {
			// assignment operator returns rvalue
			t("proc(a: ref bool){a;}(a: bool = true)", null);
		});

		// workaround: (though undefined behaviour.. but if you want to shoot yourself in the foot...)
		t("proc(a: ref bool){a;}({a: bool = true; a;})", PrimitiveType.Boolean);
	}

	@Test
	public void testSimpleProcedure() {
		t("proc foo(a: int) {a;}",
				new CallableType(new AnnotatedType[] {
						new AnnotatedType(PrimitiveType.Integer)
				}, new AnnotatedType(PrimitiveType.Integer)));
		t("proc foo(a: int) {a + 5;}",
				new CallableType(new AnnotatedType[] {
						new AnnotatedType(PrimitiveType.Integer)
				}, new AnnotatedType(PrimitiveType.Integer)));
	}
	
	@Test
	public void testAssigningProceduresToVariables() {
		t("a: <> -> void = proc (){}",
				new CallableType(new AnnotatedType[0], new AnnotatedType(PrimitiveType.Void)));
		t("alma: <int, bool> -> bool = proc (a: int, b: bool){a == a && b;}",
				new CallableType(new AnnotatedType[] {
						new AnnotatedType(PrimitiveType.Integer),
						new AnnotatedType(PrimitiveType.Boolean)
				}, new AnnotatedType(PrimitiveType.Boolean)));
		t("alma: <int, ref bool> -> bool = proc (a: int, b: bool){a == a && b;}",
				new CallableType(new AnnotatedType[] {
						new AnnotatedType(PrimitiveType.Integer),
						new AnnotatedType(PrimitiveType.Boolean, ValueCategory.LValue)
				}, new AnnotatedType(PrimitiveType.Boolean)));

		Assertions.assertThrows(TypeError.class, () -> {
			// cannot assign a function which would need more specific input
			t("alma: <int, bool> -> bool = proc (a: int, b: ref bool){a == a && b;}", null);
		});

		t("alma: <int, ref bool> -> void = proc (a: int, b: bool){a == a && b;}",
				new CallableType(new AnnotatedType[] {
						new AnnotatedType(PrimitiveType.Integer),
						new AnnotatedType(PrimitiveType.Boolean, ValueCategory.LValue)
				}, new AnnotatedType(PrimitiveType.Void)));

		Assertions.assertThrows(TypeError.class, () -> {
			// cannot assign a function which would return a more specific output
			t("alma: <int, ref bool> -> bool = proc (a: int, b: bool){}", null);
		});
	}

	@Test
	public void testCallingProceduresAssignedToVariables() {
		t("{foo: <> -> void = proc() {}; foo();}", PrimitiveType.Void);
		t("{bar: <int> -> int = proc(a: int) {a;}; bar(4);}", PrimitiveType.Integer);
		t("{a: int = 4; bar: <ref int> -> int = proc(a: ref int){a = a + 1;}; bar(a);}", PrimitiveType.Integer);
		Assertions.assertThrows(InvalidParameterTypesException.class, () -> {
			// needs a reference
			t("{bar: <ref int> -> int = proc(a: ref int){a = a + 1;}; bar(5);}", null);
		});
	}

	@Test
	public void testCorrectVariableFrames() {
		undeclared("{a: int = 3; proc(){a = a+1;};}", "a");
		t("{a: int = 3; proc(a: bool){a || false;}(true);}", PrimitiveType.Boolean);
	}

	@Test
	public void testProcedureOverloads() {
		t("{proc foo(a: int){a;}; proc foo(b: bool){b;}; foo(4);}", PrimitiveType.Integer);
		t("{proc foo(a: int){a;}; proc foo(b: bool){b;}; foo(true);}", PrimitiveType.Boolean);
		Assertions.assertThrows(NoMatchingOverloadException.class, () -> {
			t("{proc foo(a: int){a;}; proc foo(b: bool){b;}; foo(3, true);}", null);
		});
		Assertions.assertThrows(InvalidParameterTypesException.class, () -> {
			t("{proc foo(a: int, b: bool){a;}; foo(true, 3);}", null);
		});
		t("{proc foo(a: ref int){a;}; proc foo(a: int){true;}; foo(4);}", PrimitiveType.Boolean);
	}

	@Test
	public void testOverloadsInRoot() {
		TypeCheckingContext context = precompile("""
			a: bool;
			
			proc foo(a: bool) {
				a || false;
			}

			proc foo(a: bool) {
				if (a) then 1 else 0;
			}
			
			proc foo(a: int) -> bool {
				a == 4;
			}
		""");

		hasVariable(context, "a", PrimitiveType.Boolean);
		hasProcedure(context, "foo", new CallableType(
				new AnnotatedType[] { new AnnotatedType(PrimitiveType.Integer)},
				new AnnotatedType(PrimitiveType.Boolean))
		);
		hasProcedure(context, "foo", new CallableType(
				new AnnotatedType[] { new AnnotatedType(PrimitiveType.Boolean)},
				new AnnotatedType(PrimitiveType.Boolean))
		);
		hasProcedure(context, "foo", new CallableType(
				new AnnotatedType[] { new AnnotatedType(PrimitiveType.Boolean)},
				new AnnotatedType(PrimitiveType.Integer))
		);
		Assertions.assertThrows(AssertionFailedError.class, () -> {
			hasProcedure(context, "foo", new CallableType(
					new AnnotatedType[] { new AnnotatedType(PrimitiveType.Integer)},
					new AnnotatedType(PrimitiveType.Integer))
			);
		});
	}

	@Test
	public void testAssigningProcedureNamesToVariables() {
		TypeCheckingContext context = precompile("""
			proc gcd(a: int, b: int) {
				num1: int = a;
				num2: int = b;
				while (num1 != num2) do {
					if (num1 > num2) then {
						num1 = num1 - num2;
					} else {
						num2 = num2 - num1;
					};
				};
				num1;
			}
			
			proc gcdRec(a: int, b: int) -> int {
				if (b == 0) then {
					a;
				} else {
					gcdRec(b, a % b);
				};
			}

			proc lcm(a: int, b: int) -> int {
				(a * b) / gcd(a, b);
			}
			
			proc lcmOrGcd(a: int, b: int, isLcm: bool) {
				func: <int, int> -> int;
				if (isLcm) then {
					func = lcm;
				} else {
					func = gcd;
				};
				func(a, b);
			}
		""");

		hasProcedure(context, "gcd", new CallableType(
				new AnnotatedType[] { new AnnotatedType(PrimitiveType.Integer), new AnnotatedType(PrimitiveType.Integer) },
				new AnnotatedType(PrimitiveType.Integer))
		);
		hasProcedure(context, "gcdRec", new CallableType(
				new AnnotatedType[] { new AnnotatedType(PrimitiveType.Integer), new AnnotatedType(PrimitiveType.Integer) },
				new AnnotatedType(PrimitiveType.Integer))
		);
		hasProcedure(context, "lcm", new CallableType(
				new AnnotatedType[] { new AnnotatedType(PrimitiveType.Integer), new AnnotatedType(PrimitiveType.Integer) },
				new AnnotatedType(PrimitiveType.Integer))
		);
		hasProcedure(context, "lcmOrGcd", new CallableType(
				new AnnotatedType[] { new AnnotatedType(PrimitiveType.Integer), new AnnotatedType(PrimitiveType.Integer), new AnnotatedType(PrimitiveType.Boolean) },
				new AnnotatedType(PrimitiveType.Integer))
		);
	}

	private void hasVariable(TypeCheckingContext context, String name, PrimitiveType type) {
		Assertions.assertEquals(type, context.lookupVariable(name).a);
	}

	private void hasProcedure(TypeCheckingContext context, String name, CallableType type) {
		List<Pair<CallableType, Integer>> overloads = context.lookupProcedure(name);
		for (Pair<CallableType, Integer> overload : overloads) {
			if (type.equals(overload.a)) {
				return;
			}
		}

		Assertions.fail("No procedure with name " + name + " and type "
				+ type.toPrettyString() + " found");
	}

	private void undeclared(String expression, String undeclaredVariable) {
		ExpressionNode parsed = ExpressionParser.parseExpression(expression);
		TypeCheckingContext context = new TypeCheckingContext();
		context.pushProcedureFrame();
		try {
			checker.check(context, parsed);
		} catch (UndeclaredVariableException e) {
			Assertions.assertEquals(undeclaredVariable, e.getName());
			return;
		}
		Assertions.fail("No undeclared variable found");
	}

	private void t(String expression, Type result) {
		ExpressionNode parsed = ExpressionParser.parseExpression(expression);
		TypeCheckingContext context = new TypeCheckingContext();
		context.pushProcedureFrame();
		Assertions.assertEquals(result, checker.check(context, parsed).getType());
	}

	private TypeCheckingContext precompile(String expression) {
		ExpressionNode parsed = ExpressionParser.parse(expression);
		TypeCheckingContext context = new TypeCheckingContext();
		checker.check(context, parsed);
		return context;
	}
}