package interpreter.value;

import expression.ProcedureDeclarationNode;
import interpreter.exceptions.InvalidOperationError;

public class BooleanValue extends Value {

	private boolean value;

	public BooleanValue(boolean value) {
		this.value = value;
	}

	@Override
	public void setValue(Value other) {
		value = other.getBooleanValue();
	}

	@Override
	public int getIntValue() {
		throw new InvalidOperationError("Casting boolean to integer");
	}

	@Override
	public boolean getBooleanValue() {
		return value;
	}

	@Override
	public ProcedureDeclarationNode getProcedureValue() {
		return null;
	}

	@Override
	public Value duplicate() {
		return new BooleanValue(value);
	}
}
