package parser;

import expression.ExpressionNode;
import org.antlr.v4.runtime.BaseErrorListener;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;

public class ExpressionParser {

	private ExpressionParser() {
		// disabled
	}

	private static ExpressionsParser getParser(String in) {
		ExpressionsLexer l = new ExpressionsLexer(CharStreams.fromString(in));
		ExpressionsParser p = new ExpressionsParser(new CommonTokenStream(l));

		p.addErrorListener(new BaseErrorListener() {
			@Override
			public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line,
					int charPositionInLine, String msg, RecognitionException e) {
				throw new IllegalStateException("failed to parse at line " + line + " due to " + msg, e);
			}
		});

		return p;
	}

	public static ExpressionNode parse(String in) {
		return getParser(in).root().value;
	}

	public static ExpressionNode parseExpression(String in) {
		return getParser(in).expr().value;
	}
}
