package expression;

public class IdentifierNode extends ExpressionNode {

	private final String name;
	private Integer stackPosition;

	public IdentifierNode(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	@Override
	public String toPrettyString() {
		return name + (stackPosition != null ? "[" + stackPosition + "]" : "");
	}

	@Override
	public NodeType getNodeType() {
		return NodeType.Identifier;
	}

	@Override
	public ExpressionNode[] getChildren() {
		return new ExpressionNode[0];
	}

	public void setStackPosition(int stackPosition) {
		this.stackPosition = stackPosition;
	}

	public int getStackPosition() {
		return stackPosition;
	}
}
