package expression;

import interpreter.value.IntegerValue;
import interpreter.value.Value;

public class IntegerNode extends ExpressionNode {

	private final int value;

	public IntegerNode(int value) {
		this.value = value;
	}

	@Override
	public String toPrettyString() {
		return String.valueOf(value);
	}

	@Override
	public NodeType getNodeType() {
		return NodeType.Integer;
	}

	@Override
	public ExpressionNode[] getChildren() {
		return new ExpressionNode[0];
	}

	public Value getValue() {
		return new IntegerValue(value);
	}
}
