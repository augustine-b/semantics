import expression.ExpressionNode;
import interpreter.EvaluationContext;
import interpreter.Interpreter;
import interpreter.InterpreterFactory;
import interpreter.value.BooleanValue;
import interpreter.value.IntegerValue;
import interpreter.value.ProcedureValue;
import interpreter.value.Value;
import parser.ExpressionParser;
import typechecker.TypeChecker;
import typechecker.TypeCheckerFactory;
import typechecker.TypeCheckingContext;
import typechecker.type.AnnotatedType;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

public class Interactive {
	public static void main(String[] args) {
		System.out.println("Welcome to the interactive version of the programming language " +
				"designed for the SS21 Semantics course!");
		System.out.println("Type `load [filename]` to parse, type check and precompile a file");
		System.out.println("And then type various expressions, the system will evaluate them");

		TypeCheckerFactory checkerFactory = new TypeCheckerFactory();
		TypeChecker typeChecker = checkerFactory.buildTypeChecker();

		InterpreterFactory interpreterFactory = new InterpreterFactory();
		Interpreter interpreter = interpreterFactory.buildInterpreter();

		TypeCheckingContext context = new TypeCheckingContext();
		EvaluationContext evaluationContext = new EvaluationContext();

		Scanner sc = new Scanner(System.in);
		while (true) {
			String input = sc.nextLine();

			if (input.trim().startsWith("load")) {
				String filename = input.substring(input.indexOf("load") + 5).trim();
				try {
					String fileContent = loadFile(filename);

					context = new TypeCheckingContext();
					evaluationContext = new EvaluationContext();

					try {
						ExpressionNode parsed = ExpressionParser.parse(fileContent);
						typeChecker.check(context, parsed);
						interpreter.compute(evaluationContext, parsed);
						System.out.println("File loaded successfully");
					} catch (Throwable t) {
						System.err.println(t.getMessage());
					}
				} catch (IOException e) {
					System.err.println("Invalid file");
				}
			} else {
				try {
					ExpressionNode parsed = ExpressionParser.parseExpression(input);

					context.pushProcedureFrame();
					AnnotatedType type = typeChecker.check(context, parsed);
					context.popProcedureFrame();

					evaluationContext.pushFrame();
					Value value = interpreter.compute(evaluationContext, parsed);
					evaluationContext.popFrame();

					System.out.print(type.getType().toPrettyString());

					if (value instanceof IntegerValue) {
						System.out.println(": " + value.getIntValue());
					} else if (value instanceof BooleanValue) {
						System.out.println(": " + value.getBooleanValue());
					} else if (value instanceof ProcedureValue) {
						System.out.println();
						System.out.println(value.getProcedureValue().toPrettyString().replace("\n", "\t\n"));
					}
				} catch (Throwable t) {
					System.err.println(t.getMessage());
				}
			}
		}
	}

	private static String loadFile(String filename) throws IOException {
		File file = new File(filename);

		byte[] data;
		try (FileInputStream fis = new FileInputStream(file)) {
			data = new byte[(int) file.length()];
			fis.read(data);
			return new String(data, StandardCharsets.UTF_8);
		}
	}
}
