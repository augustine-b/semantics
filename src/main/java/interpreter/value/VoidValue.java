package interpreter.value;

import expression.ProcedureDeclarationNode;
import interpreter.exceptions.InvalidOperationError;

public class VoidValue extends Value {

	public static final VoidValue INSTANCE = new VoidValue();

	private VoidValue() {
		// singleton class
	}

	@Override
	public void setValue(Value other) {
		throw new InvalidOperationError("Setting value of void");
	}

	@Override
	public int getIntValue() {
		throw new InvalidOperationError("Casting void to integer");
	}

	@Override
	public boolean getBooleanValue() {
		throw new InvalidOperationError("Casting void to boolean");
	}

	@Override
	public ProcedureDeclarationNode getProcedureValue() {
		throw new InvalidOperationError("Casting void to procedure");
	}

	@Override
	public Value duplicate() {
		throw new InvalidOperationError("Duplicating void value");
	}
}
