package typechecker.exceptions;

import typechecker.type.CallableType;

import java.util.List;
import java.util.stream.Collectors;

public class AmbiguousReferenceException extends RuntimeException {
	public AmbiguousReferenceException(String name, List<CallableType> procedureTypes) {
		super("Reference to the procedure " + name + " in this context is ambiguous. " +
				"The overloads are: " + procedureTypes.stream().map(CallableType::toPrettyString)
				.collect(Collectors.joining(", ")));
	}
}
