package interpreter;

import expression.ExpressionNode;
import interpreter.value.Value;
import typechecker.type.AnnotatedType;

import java.util.Arrays;

public class InterpreterEvaluationRule {

	private TypeCheckerFn typeChecker;
	private ExpressionEvaluatorFn evaluator;

	InterpreterEvaluationRule withParameters(TypeCheckerFn typeChecker) {
		this.typeChecker = typeChecker;
		return this;
	}

	InterpreterEvaluationRule resultsIn(ExpressionEvaluatorFn evaluator) {
		this.evaluator = evaluator;
		return this;
	}

	public boolean matches(ExpressionNode expr) {
		return typeChecker.checkTypes(Arrays.stream(expr.getChildren())
				.map(ExpressionNode::getComputedType).toArray(AnnotatedType[]::new));
	}

	public Value evaluate(EvaluationContext context, ExpressionNode expr) {
		return evaluator.evaluateExpression(context, expr);
	}

	interface TypeCheckerFn {
		boolean checkTypes(AnnotatedType[] types);
	}

	interface ExpressionEvaluatorFn {
		Value evaluateExpression(EvaluationContext context, ExpressionNode node);
	}
}
