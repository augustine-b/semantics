package interpreter.value;

import expression.ProcedureDeclarationNode;

public abstract class Value {
	public abstract void setValue(Value other);

	public abstract int getIntValue();

	public abstract boolean getBooleanValue();

	public abstract ProcedureDeclarationNode getProcedureValue();

	public abstract Value duplicate();
}
