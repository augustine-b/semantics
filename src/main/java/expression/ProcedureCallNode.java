package expression;

import java.util.Arrays;
import java.util.stream.Collectors;

public class ProcedureCallNode extends ExpressionNode {

	private final ExpressionNode callableExpression;
	private final ExpressionNode[] actualParameters;

	public ProcedureCallNode(ExpressionNode callableExpression, ExpressionNode[] actualParameters) {
		this.callableExpression = callableExpression;
		this.actualParameters = actualParameters;
	}

	public ExpressionNode getCallableExpression() {
		return callableExpression;
	}

	@Override
	public String toPrettyString() {
		return callableExpression.toPrettyString() + "("
				+ Arrays.stream(actualParameters).map(ExpressionNode::toPrettyString).collect(Collectors.joining(", "))
				+ ")";
	}

	@Override
	public NodeType getNodeType() {
		return NodeType.ProcedureCall;
	}

	@Override
	public ExpressionNode[] getChildren() {
		return actualParameters;
	}
}
