package interpreter.exceptions;

public class InvalidOperationError extends Error {
	public InvalidOperationError(String cause) {
		super(cause);
	}
}
