package typechecker.exceptions;

import expression.ExpressionNode;
import typechecker.type.AnnotatedType;
import typechecker.type.CallableType;

import java.util.Arrays;

public class InvalidParameterTypesException extends RuntimeException {
	public InvalidParameterTypesException(ExpressionNode callableExpression,
			CallableType callableType, AnnotatedType[] childTypes) {
		super("The expression " + callableExpression.toPrettyString() + " has a type of "
				+ callableType.toPrettyString() + " which is not callable with parameters "
				+ Arrays.toString(childTypes));
	}
}
