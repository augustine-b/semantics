package interpreter;

import expression.ExpressionNode;
import expression.NodeType;
import interpreter.exceptions.InvalidOperationError;
import interpreter.value.Value;
import org.antlr.v4.runtime.misc.MultiMap;

import java.util.Arrays;

public class Interpreter {

	public MultiMap<NodeType, InterpreterEvaluationRule> evaluationRules;
	public MultiMap<NodeType, InterpreterComputationRule> computationRules;

	Interpreter() {
		evaluationRules = new MultiMap<>();
		computationRules = new MultiMap<>();
	}

	public Value compute(EvaluationContext context, ExpressionNode expr) {
		if (evaluationRules.get(expr.getNodeType()) != null) {
			for (InterpreterEvaluationRule evaluationRule :
					evaluationRules.get(expr.getNodeType())) {
				if (evaluationRule.matches(expr)) {
					return evaluationRule.evaluate(context, expr);
				}
			}
		}

		Value[] children = Arrays.stream(expr.getChildren())
				.map(child -> compute(context, child))
				.toArray(Value[]::new);

		for (InterpreterComputationRule computationRule :
				computationRules.get(expr.getNodeType())) {
			if (computationRule.matches(expr)) {
				return computationRule.compute(expr, children);
			}
		}

		throw new InvalidOperationError("No matching interpreter rule for " + expr.toPrettyString());
	}

	public InterpreterComputationRule addComputer(NodeType nodeType) {
		InterpreterComputationRule computationRule = new InterpreterComputationRule();
		computationRules.map(nodeType, computationRule);
		return computationRule;
	}

	public InterpreterEvaluationRule addEvaluator(NodeType nodeType) {
		InterpreterEvaluationRule evaluationRule = new InterpreterEvaluationRule();
		evaluationRules.map(nodeType, evaluationRule);
		return evaluationRule;
	}
}
