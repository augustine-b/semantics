package interpreter;

import expression.BooleanNode;
import expression.DeclarationNode;
import expression.IdentifierNode;
import expression.IntegerNode;
import expression.NodeType;
import expression.ProcedureCallNode;
import expression.ProcedureDeclarationNode;
import interpreter.value.BooleanValue;
import interpreter.value.IntegerValue;
import interpreter.value.ProcedureValue;
import interpreter.value.Value;
import interpreter.value.VoidValue;
import typechecker.ValueCategory;
import typechecker.type.PrimitiveType;

import java.util.Arrays;
import java.util.function.BiFunction;

public class InterpreterFactory {

	public Interpreter buildInterpreter() {
		Interpreter interpreter = new Interpreter();

		interpreter.addComputer(NodeType.Root)
				.withComputer((node, children) -> VoidValue.INSTANCE);

		interpreter.addEvaluator(NodeType.ProcedureDeclaration)
				.withParameters(types -> true)
				.resultsIn((context, node) -> {
					ProcedureDeclarationNode declarationNode = (ProcedureDeclarationNode) node;
					ProcedureValue procedureValue = new ProcedureValue(declarationNode);
					context.allocateVariable(
							declarationNode.getStackPosition(), procedureValue);
					return procedureValue;
				});

		interpreter.addEvaluator(NodeType.ProcedureCall)
				.withParameters(types -> true)
				.resultsIn((context, node) -> {
					ProcedureCallNode callNode = (ProcedureCallNode) node;
					Value callableValue = interpreter.compute(context, callNode.getCallableExpression());

					Value[] actualParameters = Arrays.stream(callNode.getChildren())
							.map(param -> interpreter.compute(context, param))
							.toArray(Value[]::new);

					context.pushFrame();

					ProcedureDeclarationNode declaration = callableValue.getProcedureValue();

					// for recursive procedures we have the procedure at the zeroth position
					context.allocateVariable(0, new ProcedureValue(declaration));

					DeclarationNode[] parameters = declaration.getParameters();
					for (int i = 0; i < parameters.length; i++) {
						DeclarationNode parameter = parameters[i];

						Value actualParameter = actualParameters[i];
						if (parameter.getType().getCategory() == ValueCategory.RValue) {
							actualParameter = actualParameter.duplicate();
						}

						context.allocateVariable(parameter.getStackPosition(), actualParameter);
					}

					Value result = interpreter.compute(context, declaration.getBody());

					context.popFrame();

					if (declaration.getReturnType().getCategory() == ValueCategory.RValue) {
						return result.duplicate();
					}

					return result;
				});

		interpreter.addEvaluator(NodeType.Declaration)
				.withParameters(types -> true)
				.resultsIn((context, node) -> {
					DeclarationNode declarationNode = (DeclarationNode) node;
					return context.allocateVariable(declarationNode.getStackPosition(),
							declarationNode.getType().getType());
				});

		interpreter.addComputer(NodeType.Assignment)
				.withComputer((node, children) -> {
					children[0].setValue(children[1]);
					return children[0];
				});

		interpreter.addEvaluator(NodeType.Identifier)
				.withParameters(types -> true)
				.resultsIn((context, node) -> {
					IdentifierNode identifierNode = (IdentifierNode) node;
					return context.getVariable(identifierNode.getStackPosition());
				});

		interpreter.addEvaluator(NodeType.While)
				.withParameters(types -> true)
				.resultsIn((context, node) -> {
					Value result = node.getChildren()[1].getComputedType().getType().defaultValue();
					while (interpreter.compute(context, node.getChildren()[0]).getBooleanValue()) {
						result = interpreter.compute(context, node.getChildren()[1]);
					}

					return result;
				});

		interpreter.addEvaluator(NodeType.If)
				.withParameters((types) ->
						types.length == 2 && types[0].getType() == PrimitiveType.Boolean
				).resultsIn((context, node) -> {
					Value condition = interpreter.compute(context, node.getChildren()[0]);
					if (condition.getBooleanValue()) {
						return interpreter.compute(context, node.getChildren()[1]);
					} else {
						return node.getChildren()[1].getComputedType().getType().defaultValue();
					}
				});

		interpreter.addEvaluator(NodeType.If)
				.withParameters((types) ->
						types.length == 3 && types[0].getType() == PrimitiveType.Boolean
				).resultsIn((context, node) -> {
					Value condition = interpreter.compute(context, node.getChildren()[0]);
					if (condition.getBooleanValue()) {
						return interpreter.compute(context, node.getChildren()[1]);
					} else {
						return interpreter.compute(context, node.getChildren()[2]);
					}
				});

		interpreter.addComputer(NodeType.Block)
				.withComputer((node, children) ->
						children.length > 0
								? children[children.length - 1]
								: VoidValue.INSTANCE
				);

		interpreter.addComputer(NodeType.Integer)
				.withComputer((node, children) -> ((IntegerNode) node).getValue());

		interpreter.addComputer(NodeType.Boolean)
				.withComputer((node, children) -> ((BooleanNode) node).getValue());

		addIntegerOperation(interpreter, NodeType.Plus, Integer::sum);
		addIntegerOperation(interpreter, NodeType.Minus, (a, b) -> a - b);
		addIntegerOperation(interpreter, NodeType.Multiply, (a, b) -> a * b);
		addIntegerOperation(interpreter, NodeType.Divide, (a, b) -> a / b);
		addIntegerOperation(interpreter, NodeType.Modulo, (a, b) -> a % b);

		interpreter.addComputer(NodeType.Minus)
				.withComputer((node, children) -> new IntegerValue(-children[0].getIntValue()));

		addBooleanOperation(interpreter, NodeType.Equals, (a, b) -> a == b);
		addBooleanOperation(interpreter, NodeType.NotEquals, (a, b) -> a != b);
		addBooleanOperation(interpreter, NodeType.And, (a, b) -> a && b);
		addBooleanOperation(interpreter, NodeType.Or, (a, b) -> a || b);

		interpreter.addComputer(NodeType.Not)
				.withComputer((node, children) -> new BooleanValue(!children[0].getBooleanValue()));

		addComparisonOperation(interpreter, NodeType.Equals, Integer::equals);
		addComparisonOperation(interpreter, NodeType.NotEquals, (a, b) -> !a.equals(b));
		addComparisonOperation(interpreter, NodeType.Less, (a, b) -> a < b);
		addComparisonOperation(interpreter, NodeType.LessEquals, (a, b) -> a <= b);
		addComparisonOperation(interpreter, NodeType.Greater, (a, b) -> a > b);
		addComparisonOperation(interpreter, NodeType.GreaterEquals, (a, b) -> a >= b);

		return interpreter;
	}

	private void addComparisonOperation(Interpreter interpreter, NodeType nodeType,
			BiFunction<Integer, Integer, Boolean> computer) {
		interpreter.addComputer(nodeType)
				.withParameters(PrimitiveType.Integer, PrimitiveType.Integer)
				.withComputer((node, children) -> {
					int val0 = children[0].getIntValue();
					int val1 = children[1].getIntValue();
					return new BooleanValue(computer.apply(val0, val1));
				});
	}

	private void addIntegerOperation(Interpreter interpreter, NodeType nodeType,
			BiFunction<Integer, Integer, Integer> computer) {
		interpreter.addComputer(nodeType)
				.withParameters(PrimitiveType.Integer, PrimitiveType.Integer)
				.withComputer((node, children) -> {
					int val0 = children[0].getIntValue();
					int val1 = children[1].getIntValue();
					return new IntegerValue(computer.apply(val0, val1));
				});
	}

	private void addBooleanOperation(Interpreter interpreter, NodeType nodeType,
			BiFunction<Boolean, Boolean, Boolean> computer) {
		interpreter.addComputer(nodeType)
				.withParameters(PrimitiveType.Boolean, PrimitiveType.Boolean)
				.withComputer((node, children) -> {
					boolean val0 = children[0].getBooleanValue();
					boolean val1 = children[1].getBooleanValue();
					return new BooleanValue(computer.apply(val0, val1));
				});
	}
}
