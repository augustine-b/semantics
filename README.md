## Formal Semantics of Programming Languages

### Parser
Written in ANTLR4

### Type Checker
`TypeCheckerRule`s created by `TypeCheckerFactory` applied by `TypeChecker`

Example of a rule:
```java
typeChecker.setHasFrame(NodeType.While);
typeChecker.addCheck(NodeType.While)
        .withParameters((context, node, types) ->
            types.length == 2 && types[0].getType() == PrimitiveType.Boolean
        ).resultsIn((context, node, types) -> types[1].asRValue());
```

Example of a batch of similar rules:
```java
addIntegerOperation(typeChecker, NodeType.Plus);
addIntegerOperation(typeChecker, NodeType.Minus);
addIntegerOperation(typeChecker, NodeType.Multiply);
addIntegerOperation(typeChecker, NodeType.Divide);
addIntegerOperation(typeChecker, NodeType.Modulo);
```

```java
private void addIntegerOperation(TypeChecker typeChecker, NodeType nodeType) {
    typeChecker.addCheck(nodeType)
            .withParameters(PrimitiveType.Integer, PrimitiveType.Integer)
            .resultsIn(PrimitiveType.Integer);
}
```

Type checker main loop:
```java
for (TypeCheckerRule rule : rules.get(expression.getNodeType())) {
    AnnotatedType type = rule.matches(context, expression, childTypes);
    if (type != null) {
        rule.executeSideEffect(context, expression, childTypes);

        if (hasOwnFrame.contains(expression.getNodeType())) {
            context.popSimpleFrame();
        }

        expression.setComputedType(type);
        return type;
    }
}
```

###Interpreter
Follows a very similar structure to the type checker: `InterpreterEvaluationRule`s 
and `InterpreterComputationRule`s created by `InterpreterFactory` used by `Interpreter`.

Example of evaluation rule:
```java
interpreter.addEvaluator(NodeType.Declaration)
        .withParameters(types -> true)
        .resultsIn((context, node) -> {
            DeclarationNode declarationNode = (DeclarationNode) node;
            return context.allocateVariable(declarationNode.getStackPosition(), 
                    declarationNode.getType().getType());
        });
```
Let's go back to the type checker rule of `Declaration` node type:
```java
typeChecker.addCheck(NodeType.Declaration)
        .withSideEffect((context, node, types) -> {
            DeclarationNode declarationNode = (DeclarationNode) node;
            int stackPosition = context.addVariable(declarationNode.getName(),
                    declarationNode.getType().getType());
            declarationNode.setStackPosition(stackPosition);
        }).resultsIn(((context, node, types) -> {
            DeclarationNode declarationNode = (DeclarationNode) node;
            return declarationNode.getType().asLValue();
        }));
```
The context keeps track of the (absolute [for global] and relative [for local]) 
positions of the variables, and you can see how these values are assigned to the
nodes in the syntax tree. Then we only use these positions during the interpretation.

In `EvaluationContext` we have
```java
private final Value[] staticMemory;
private final Value[] stack;

private int basePointer;
private int stackPointer;
```
and these are used in the following ways:
```java
public void pushFrame() {
    if (stackPointer >= STACK_SIZE) {
        throw new RuntimeException("Stack overflow!");
    }
    stack[stackPointer] = new IntegerValue(basePointer);
    basePointer = ++stackPointer;
}

public void popFrame() {
    assert stackPointer >= 0;
    stackPointer = basePointer;
    basePointer = stack[basePointer - 1].getIntValue();
}
```
```java
public void allocateVariable(int position, Value value) {
    if (stackPointer >= STACK_SIZE) {
        throw new RuntimeException("Stack overflow!");
    }

    assert basePointer + position == stackPointer;
    stack[stackPointer] = value;
    stackPointer++;
}
```
