package expression;

import typechecker.type.AnnotatedType;

import java.util.Arrays;

public class ProcedureDeclarationNode extends ExpressionNode {

	private final String name;
	private final DeclarationNode[] parameters;
	private AnnotatedType returnType;
	private final ExpressionNode body;
	private Integer stackPosition;

	public ProcedureDeclarationNode(String name, DeclarationNode[] parameters,
			AnnotatedType returnType, ExpressionNode body) {
		this.name = name;
		this.parameters = parameters;
		this.returnType = returnType;
		this.body = body;
	}

	public String getName() {
		return name;
	}

	public DeclarationNode[] getParameters() {
		return parameters;
	}

	public AnnotatedType getReturnType() {
		return returnType;
	}

	public ExpressionNode getBody() {
		return body;
	}

	@Override
	public String toPrettyString() {
		StringBuilder sb = new StringBuilder();
		if (name != null) {
			sb.append("proc ").append(name);
			if (stackPosition != null) {
				sb.append("[").append(stackPosition).append("]");
			}
			sb.append("(");
		} else {
			sb.append("proc(");
		}
		for (int i = 0; i < parameters.length; i++) {
			if (i != 0) {
				sb.append(", ");
			}
			sb.append(parameters[i].toPrettyString());
		}
		sb.append(") ");
		if (returnType != null) {
			sb.append("-> ").append(returnType.toPrettyString()).append(" ");
		}
		sb.append(body.toPrettyString());
		return sb.toString();
	}

	@Override
	public NodeType getNodeType() {
		return NodeType.ProcedureDeclaration;
	}

	@Override
	public ExpressionNode[] getChildren() {
		return new ExpressionNode[] { body };
	}

	public AnnotatedType[] getInputTypes() {
		return Arrays.stream(parameters).map(DeclarationNode::getType).toArray(AnnotatedType[]::new);
	}

	public void setStackPosition(Integer stackPosition) {
		this.stackPosition = stackPosition;
	}

	public Integer getStackPosition() {
		return stackPosition;
	}

	public void setReturnType(AnnotatedType returnType) {
		this.returnType = returnType;
	}
}
