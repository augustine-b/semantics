package typechecker.exceptions;

public class UndeclaredVariableException extends RuntimeException {
	private final String name;

	public UndeclaredVariableException(String name) {
		super("Undeclared variable: " + name);
		this.name = name;
	}

	public String getName() {
		return name;
	}
}
