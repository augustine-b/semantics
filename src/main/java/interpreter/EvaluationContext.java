package interpreter;

import interpreter.exceptions.InvalidOperationError;
import interpreter.value.BooleanValue;
import interpreter.value.IntegerValue;
import interpreter.value.ProcedureValue;
import interpreter.value.Value;
import typechecker.type.PrimitiveType;
import typechecker.type.Type;

public class EvaluationContext {

	private static final int STATIC_MEMORY_SIZE = 100;
	private static final int STACK_SIZE = 500;

	private final Value[] staticMemory;
	private final Value[] stack;

	private int basePointer;
	private int stackPointer;

	public EvaluationContext() {
		staticMemory = new Value[STATIC_MEMORY_SIZE];
		stack = new Value[STACK_SIZE];
	}

	public void pushFrame() {
		if (stackPointer >= STACK_SIZE) {
			throw new RuntimeException("Stack overflow!");
		}
		stack[stackPointer] = new IntegerValue(basePointer);
		basePointer = ++stackPointer;
	}

	public void popFrame() {
		assert stackPointer >= 0;
		stackPointer = basePointer;
		basePointer = stack[basePointer - 1].getIntValue();
	}

	public Value allocateVariable(int position, Type type) {
		Value value;

		if (type instanceof PrimitiveType) {
			value = switch ((PrimitiveType) type) {
				case Integer -> new IntegerValue(0);
				case Boolean -> new BooleanValue(false);
				default -> throw new InvalidOperationError("Creating variable of unknown type");
			};
		} else {
			value = new ProcedureValue(null);
		}

		allocateVariable(position, value);

		return value;
	}

	public Value getVariable(int position) {
		if (position < 0) {
			return staticMemory[-position];
		} else {
			return stack[basePointer + position];
		}
	}

	public void allocateVariable(int position, Value value) {
		if (position < 0) {
			if (-position > STATIC_MEMORY_SIZE) {
				throw new RuntimeException("Static memory limit exceeded!");
			}

			staticMemory[-position] = value;
		} else {
			if (stackPointer >= STACK_SIZE) {
				throw new RuntimeException("Stack overflow!");
			}

			assert basePointer + position == stackPointer;
			stack[stackPointer] = value;
			stackPointer++;
		}
	}
}
