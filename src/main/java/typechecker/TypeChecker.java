package typechecker;

import expression.DeclarationNode;
import expression.ExpressionNode;
import expression.IdentifierNode;
import expression.NodeType;
import expression.ProcedureCallNode;
import expression.ProcedureDeclarationNode;
import org.antlr.v4.runtime.misc.MultiMap;
import org.antlr.v4.runtime.misc.Pair;
import typechecker.exceptions.AmbiguousReferenceException;
import typechecker.exceptions.FunctionReturnTypeMismatchException;
import typechecker.exceptions.InvalidParameterTypesException;
import typechecker.exceptions.NoMatchingOverloadException;
import typechecker.exceptions.NonCallableExpressionException;
import typechecker.exceptions.ReturnTypeDeductionException;
import typechecker.exceptions.TypeError;
import typechecker.exceptions.UndeclaredVariableException;
import typechecker.type.AnnotatedType;
import typechecker.type.CallableType;
import typechecker.type.PrimitiveType;
import typechecker.type.Type;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class TypeChecker {

	public MultiMap<NodeType, TypeCheckerRule> rules;
	public List<NodeType> hasOwnFrame;

	public TypeChecker() {
		rules = new MultiMap<>();
		hasOwnFrame = new ArrayList<>();
	}

	public AnnotatedType check(TypeCheckingContext context, ExpressionNode expression) {
		// procedure declarations and calls are too complicated, trying to implement
		// them in a more generic way is not worth it
		if (expression instanceof ProcedureDeclarationNode) {
			AnnotatedType annotatedType = handleProcedureDeclaration(context, (ProcedureDeclarationNode) expression);
			expression.setComputedType(annotatedType);
			return annotatedType;
		}

		if (hasOwnFrame.contains(expression.getNodeType())) {
			context.pushSimpleFrame();
		}

		AnnotatedType[] childTypes = Arrays.stream(expression.getChildren())
				.map(child -> this.check(context, child))
				.toArray(AnnotatedType[]::new);

		if (expression instanceof ProcedureCallNode) {
			AnnotatedType annotatedType = handleProcedureCall(context, (ProcedureCallNode) expression, childTypes);
			expression.setComputedType(annotatedType);
			return annotatedType;
		}

		for (TypeCheckerRule rule : rules.get(expression.getNodeType())) {
			AnnotatedType type = rule.matches(context, expression, childTypes);
			if (type != null) {
				rule.executeSideEffect(context, expression, childTypes);

				if (hasOwnFrame.contains(expression.getNodeType())) {
					context.popSimpleFrame();
				}

				expression.setComputedType(type);
				return type;
			}
		}

		throw new TypeError(expression, childTypes);
	}

	private AnnotatedType handleProcedureDeclaration(TypeCheckingContext context,
			ProcedureDeclarationNode declarationNode) {
		String name = declarationNode.getName();
		DeclarationNode[] parameters = declarationNode.getParameters();
		AnnotatedType[] inputTypes = declarationNode.getInputTypes();
		AnnotatedType returnType = declarationNode.getReturnType();

		CallableType functionType;
		if (returnType != null) {
			functionType = new CallableType(inputTypes, returnType);

			int stackPosition = context.addProcedure(name, functionType);
			declarationNode.setStackPosition(stackPosition);

			context.pushProcedureFrame();
			// add it also to the internal frame
			context.addProcedure(name, functionType);

			pushVariables(context, parameters);

			AnnotatedType bodyType = check(context, declarationNode.getBody());
			if (!returnType.supertypeOf(bodyType)) {
				throw new FunctionReturnTypeMismatchException(returnType, bodyType);
			}
			context.popProcedureFrame();
		} else {
			context.pushProcedureFrame();

			context.addVariable("<<dummy>>", PrimitiveType.Void);
			pushVariables(context, parameters);

			try {
				AnnotatedType bodyType = check(context, declarationNode.getBody()).asRValue();
				declarationNode.setReturnType(bodyType);

				functionType = new CallableType(inputTypes, bodyType);
				context.popProcedureFrame();

				int stackPosition = context.addProcedure(name, functionType);
				declarationNode.setStackPosition(stackPosition);
			} catch (UndeclaredVariableException e) {
				if (Objects.equals(e.getName(), declarationNode.getName())) {
					throw new ReturnTypeDeductionException(declarationNode.getName());
				} else {
					throw e;
				}
			}
		}

		return new AnnotatedType(functionType);
	}

	private void pushVariables(TypeCheckingContext context, DeclarationNode[] parameters) {
		for (DeclarationNode node : parameters) {
			int stackPosition = context.addVariable(node.getName(), node.getType().getType());
			node.setStackPosition(stackPosition);
		}
	}

	private AnnotatedType handleProcedureCall(TypeCheckingContext context,
			ProcedureCallNode procedureCallNode, AnnotatedType[] childTypes) {
		ExpressionNode callableExpression = procedureCallNode.getCallableExpression();

		CallableType callableType;
		if (callableExpression instanceof IdentifierNode) {
			IdentifierNode identifierNode = (IdentifierNode) callableExpression;
			Pair<CallableType, Integer> typeInformation = getCallableType(context, childTypes, identifierNode);
			identifierNode.setStackPosition(typeInformation.b);
			callableType = typeInformation.a;
		} else {
			AnnotatedType expressionType = check(context, callableExpression);
			if (!(expressionType.getType() instanceof CallableType)) {
				throw new NonCallableExpressionException(callableExpression, expressionType.getType());
			}
			callableType = (CallableType) expressionType.getType();
		}

		if (!callableType.callableWith(childTypes)) {
			throw new InvalidParameterTypesException(callableExpression, callableType, childTypes);
		}

		return callableType.getOutputType();
	}

	private Pair<CallableType, Integer> getCallableType(TypeCheckingContext context, AnnotatedType[] childTypes,
			IdentifierNode identifierNode) {
		String name = identifierNode.getName();
		try {
			Pair<Type, Integer> variableType = context.lookupVariable(name);
			if (!(variableType.a instanceof CallableType)) {
				throw new NonCallableExpressionException(identifierNode, variableType.a);
			}
			return new Pair<>((CallableType) variableType.a, variableType.b);
		} catch (UndeclaredVariableException | AmbiguousReferenceException e) {
			// no problem yet, it might be a procedure in one of the visible contexts
		}

		List<Pair<CallableType, Integer>> procedures = context.lookupProcedure(name);
		for (Pair<CallableType, Integer> procedure : procedures) {
			if (procedure.a.callableWith(childTypes)) {
				return procedure;
			}
		}

		throw new NoMatchingOverloadException(name, childTypes, getFirst(procedures));
	}

	private <T, U> List<T> getFirst(List<Pair<T, U>> input) {
		return input.stream().map(p -> p.a).collect(Collectors.toList());
	}

	TypeCheckerRule addCheck(NodeType nodeType) {
		TypeCheckerRule rule = new TypeCheckerRule();
		rules.map(nodeType, rule);
		return rule;
	}

	void setHasFrame(NodeType nodeType) {
		hasOwnFrame.add(nodeType);
	}
}
