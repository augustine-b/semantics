package parser;

import expression.ExpressionNode;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class ExpressionParserTest {

	@Test
	public void testSimpleExpressions() {
		t("3+5", "(3 + 5)");
		t("true || false", "(true || false)");
		t("a || b && c", "(a || (b && c))");
		t("a && b || c", "((a && b) || c)");
		t("a && (b || c)", "(a && (b || c))");
		t("3*4+5 == 17 && 5 < 6", "((((3 * 4) + 5) == 17) && (5 < 6))");
	}

	@Test
	public void testSimpleBlocks() {
		t("{}", "{\n}");
		t("{ 3 + 3; }", "{\n\t(3 + 3);\n}");
		t("{ a + b; }", "{\n\t(a + b);\n}");
		t("{ a + b; 3 < a / 2; }", "{\n\t(a + b);\n\t(3 < (a / 2));\n}");
	}

	@Test
	public void testDeclaration() {
		t("a: bool", "a: bool");
		t("b: int", "b: int");
		t("b: int = 7 * 8 - 56", "(b: int = ((7 * 8) - 56))");
	}

	@Test
	public void testIfThen() {
		t("if (3 < 4) then { a = a + 1; }", "if (3 < 4) then {\n\t(a = (a + 1));\n}");
		t("if {a = 5; a < 6;} then {a = a + 1;}", "if {\n\t(a = 5);\n\t(a < 6);\n} then {\n\t(a = (a + 1));\n}");
	}

	@Test
	public void testIfThenElse() {
		t("if true then false else true", "if true then false else true");
		t("if a then if b then c else d else e", "if a then if b then c else d else e");
		t("if a then b else if c then d else e", "if a then b else if c then d else e");
	}

	@Test
	public void testWhile() {
		t("while true do false", "while true do false");
		t("{ i: int = 0; while i < 10 do i = i + 1; }", "{\n\t(i: int = 0);\n\t(while (i < 10) do i = (i + 1));\n}");
		t("while {i: int = 0; i < 10;} do i = i + 1", "(while {\n\t(i: int = 0);\n\t(i < 10);\n} do i = (i + 1))");
	}

	@Test
	public void testProcedure() {
		t("proc alma() {}", "proc alma() {\n}");
		t("proc foo(a: int) {}", "proc foo(a: int) {\n}");
		t("proc bar(a: bool, b: int) {}", "proc bar(a: bool, b: int) {\n}");
		t("proc bar(a: bool, b: ref int) {}", "proc bar(a: bool, b: ref int) {\n}");
		t("proc bar(a: ref bool, b: int) {}", "proc bar(a: ref bool, b: int) {\n}");
		t("proc bar(a: ref bool, b: ref int) {}", "proc bar(a: ref bool, b: ref int) {\n}");
	}

	@Test
	public void testImmediatelyCalledProcedure() {
		t("proc(){}()", "proc() {\n}()");
		t("proc(a: int){}(5)", "proc(a: int) {\n}(5)");
		t("proc(a: bool){a || false;}(true)", "proc(a: bool) {\n\t(a || false);\n}(true)");
		t("proc(a: bool, b: int){x: bool = a || false; b + 3;}(true, 42)",
				"proc(a: bool, b: int) {\n\t(x: bool = (a || false));\n\t(b + 3);\n}(true, 42)");
		t("proc alma(a: int){}(5)", "proc alma(a: int) {\n}(5)");
	}

	@Test
	public void testAssigningProceduresToVariables() {
		t("a: <> -> void = proc (){}", "(a: <> -> void = proc() {\n})");
		t("alma: <int, bool> -> bool = proc (a: int, b: bool){a == a && b;}",
				"(alma: <int, bool> -> bool = proc(a: int, b: bool) {\n\t((a == a) && b);\n})");
	}

	private void t(String original, String result) {
		ExpressionNode parsed = ExpressionParser.parseExpression(original);
		String serialized = parsed.toPrettyString();
		Assertions.assertEquals(result, serialized);
	}
}