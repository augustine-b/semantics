package typechecker;

import expression.ExpressionNode;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import parser.ExpressionParser;

class StackPositionTest {

	private static TypeChecker checker;

	@BeforeAll
	public static void setupChecker() {
		TypeCheckerFactory checkerFactory = new TypeCheckerFactory();
		checker = checkerFactory.buildTypeChecker();
	}

	@Test
	public void testStackPositionOfVariables() {
		ExpressionNode precompiled = precompile("""
			proc foo() {
				a: int = 0;
				a;
			}

			baz: bool;

			proc bar(a: int, b: int, c: bool) {
				a1: int = a;
				a2: int = b;
				
				if (c || baz) then {
					d: int = a1 - a2;
					foo();
				} else {
					e: int = a1 % a2;
					d: int = a1 / a2;
				};
				
				a1 + a2;
			}
		""");

		Assertions.assertEquals("""
			proc foo[-1]() -> int {
				(a[1]: int = 0);
				a[1];
			}
			baz[-2]: bool
			proc bar[-3](a[1]: int, b[2]: int, c[3]: bool) -> int {
				(a1[4]: int = a[1]);
				(a2[5]: int = b[2]);
				if (c[3] || baz[-2]) then {
					(d[6]: int = (a1[4] - a2[5]));
					foo[-1]();
				} else {
					(e[6]: int = (a1[4] % a2[5]));
					(d[7]: int = (a1[4] / a2[5]));
				};
				(a1[4] + a2[5]);
			}""", precompiled.toPrettyString());
	}


	@Test
	public void testStackPositionOfRecursiveProcedure() {
		ExpressionNode precompiled = precompile("""
			proc fibonacci(n: int) -> int {
				if (n <= 1) then {
					1;
				} else {
					fibonacci(n - 1) + fibonacci(n - 2);
				};
			}
		""");

		Assertions.assertEquals("""
			proc fibonacci[-1](n[1]: int) -> int {
				if (n[1] <= 1) then {
					1;
				} else {
					(fibonacci[0]((n[1] - 1)) + fibonacci[0]((n[1] - 2)));
				};
			}""", precompiled.toPrettyString());
	}

	private ExpressionNode precompile(String expression) {
		ExpressionNode parsed = ExpressionParser.parse(expression);
		TypeCheckingContext context = new TypeCheckingContext();
		checker.check(context, parsed);
		return parsed;
	}
}