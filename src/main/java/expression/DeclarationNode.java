package expression;

import typechecker.type.AnnotatedType;

public class DeclarationNode extends ExpressionNode {

	private final String name;
	private final AnnotatedType type;
	private Integer stackPosition;

	public DeclarationNode(String name, AnnotatedType type) {
		this.name = name;
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public AnnotatedType getType() {
		return type;
	}

	@Override
	public String toPrettyString() {
		return name + (stackPosition != null ? "[" + stackPosition + "]" : "") + ": " + type.toPrettyString();
	}

	@Override
	public NodeType getNodeType() {
		return NodeType.Declaration;
	}

	@Override
	public ExpressionNode[] getChildren() {
		return new ExpressionNode[0];
	}

	public void setStackPosition(int stackPosition) {
		this.stackPosition = stackPosition;
	}

	public int getStackPosition() {
		return stackPosition;
	}
}
