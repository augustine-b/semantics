package typechecker.exceptions;

import expression.ExpressionNode;
import typechecker.type.AnnotatedType;

import java.util.Arrays;

public class TypeError extends Error {
	public TypeError(ExpressionNode expression, AnnotatedType[] childTypes) {
		super("Failed to match " + expression.toPrettyString()
				+ " with child types: " + Arrays.toString(childTypes));
	}
}
