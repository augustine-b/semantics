package expression;

import java.util.Arrays;

public class OperatorNode extends ExpressionNode {

	private final NodeType nodeType;
	private final ExpressionNode[] children;

	public OperatorNode(NodeType nodeType, ExpressionNode... children) {
		this.nodeType = nodeType;
		this.children = children;
	}

	@Override
	public String toPrettyString() {
		String[] childStrings = Arrays.stream(children)
				.map(ExpressionNode::toPrettyString).toArray(String[]::new);

		return nodeType.toPrettyString(childStrings);
	}

	@Override
	public NodeType getNodeType() {
		return nodeType;
	}

	@Override
	public ExpressionNode[] getChildren() {
		return children;
	}
}
