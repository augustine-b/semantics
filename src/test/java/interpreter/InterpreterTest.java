package interpreter;

import expression.ExpressionNode;
import interpreter.value.Value;
import org.antlr.v4.runtime.misc.Pair;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import parser.ExpressionParser;
import typechecker.TypeChecker;
import typechecker.TypeCheckerFactory;
import typechecker.TypeCheckingContext;
import typechecker.type.AnnotatedType;
import typechecker.type.PrimitiveType;

class InterpreterTest {

	private static TypeChecker checker;
	private static Interpreter interpreter;

	@BeforeAll
	public static void setupTests() {
		TypeCheckerFactory checkerFactory = new TypeCheckerFactory();
		checker = checkerFactory.buildTypeChecker();
		InterpreterFactory interpreterFactory = new InterpreterFactory();
		interpreter = interpreterFactory.buildInterpreter();
	}

	@Test
	public void testSimpleExpressions() {
		t("3 + 2", 5);
		t("2 + 3 * 4", 14);
		t("true || false", true);
		t("true && false", false);
		t("3 == 3 || 3 == 4", true);
	}

	@Test
	public void testIfThenElse() {
		t("if 3 == 4 then 3", 0);
		t("if 3 == 3 then 3", 3);
		t("if 3 + 1 == 4 then 12 else 13", 12);
		t("if 3 + 1 == 5 then 12 else 13", 13);
	}

	@Test
	public void testVariableAssignments() {
		t("""
		{
			num1: int = 252;
			num2: int = 105;
			while (num1 != num2) do {
				if (num1 > num2) then {
					num1 = num1 - num2;
				} else {
					num2 = num2 - num1;
				};
			};
			num1;
		}
		""", 21);
	}

	@Test
	public void testRecursiveProcedures() {
		t("""
			proc fibonacci(n: int) -> int {
				if (n <= 1) then {
					1;
				} else {
					fibonacci(n - 1) + fibonacci(n - 2);
				};
			}(10)
		""", 89);

		t("""
			proc gcd(a: int, b: int) -> int {
				if (b == 0) then {
					a;
				} else {
					gcd(b, a % b);
				};
			}(252, 105)
		""", 21);
	}

	@Test
	public void testAssigningProceduresToVariables() {
		Pair<ExpressionNode, TypeCheckingContext> precompiled = precompile("""
			proc gcd(a: int, b: int) -> int {
				if (b == 0) then {
					a;
				} else {
					gcd(b, a % b);
				};
			}

			proc lcm(a: int, b: int) {
				(a * b) / gcd(a, b);
			}
			
			proc lcmOrGcd(a: int, b: int, isLcm: bool) {
				func: <int, int> -> int;
				if (isLcm) then {
					func = lcm;
				} else {
					func = gcd;
				};
				func(a, b);
			}
		""");

		Assertions.assertEquals(42, evaluate(precompiled, "lcmOrGcd(21, 14, true)").getIntValue());
		Assertions.assertEquals(21, evaluate(precompiled, "lcmOrGcd(252, 105, false)").getIntValue());
	}

	@Test
	public void testReferenceVariables() {
		Pair<ExpressionNode, TypeCheckingContext> precompiled = precompile("""
			proc foo(a: int, b: ref int, c: bool, d: ref bool) -> void {
				a = a + 1;
				b = b + 1;
				c = true;
				d = true;
			}
		""");

		Value value = evaluate(precompiled, """
			{
				a1: int = 2;
				b1: int = 2;
				c1: bool = false;
				d1: bool = false;
				foo(a1, b1, c1, d1);
				a1 == 2 && b1 == 3 && c1 == false && d1 == true;
			}
		""");

		Assertions.assertTrue(value.getBooleanValue());
	}

	@Test
	public void testProcedureReturningReference() {
		Pair<ExpressionNode, TypeCheckingContext> precompiled = precompile("""
			proc foo(a: ref int, b: ref int, first: bool) -> ref int {
				if (first) then a else b;
			}
		""");

		Value value = evaluate(precompiled, """
			{
				a1: int;
				b1: int;
				
				foo(a1, b1, true) = 7;
				foo(a1, b1, false) = 11;
				
				a1 == 7 && b1 == 11;
			}
		""");

		Assertions.assertTrue(value.getBooleanValue());
	}

	@Test
	public void testStackOverflow() {
		Pair<ExpressionNode, TypeCheckingContext> precompiled = precompile("""
			proc fibonacci(n: int) -> int {
				if (n <= 2) then 1 else (fibonacci(n - 1) + fibonacci(n - 2));
			}
		""");

		Value value = evaluate(precompiled, "fibonacci(12)");
		Assertions.assertEquals(144, value.getIntValue());
		Assertions.assertThrows(RuntimeException.class,
				() -> evaluate(precompiled, "fibonacci(500)"));
	}

	private Value evaluate(Pair<ExpressionNode, TypeCheckingContext> precompiled,
			String expression) {
		ExpressionNode parsedExpression = ExpressionParser.parseExpression(expression);

		precompiled.b.pushProcedureFrame();
		checker.check(precompiled.b, parsedExpression);
		precompiled.b.popProcedureFrame();

		EvaluationContext evaluationContext = new EvaluationContext();
		interpreter.compute(evaluationContext, precompiled.a);

		evaluationContext.pushFrame();
		return interpreter.compute(evaluationContext, parsedExpression);
	}

	private Pair<ExpressionNode, TypeCheckingContext> precompile(String expression) {
		ExpressionNode parsed = ExpressionParser.parse(expression);
		TypeCheckingContext context = new TypeCheckingContext();
		checker.check(context, parsed);
		return new Pair<>(parsed, context);
	}

	private void t(String expression, int expected) {
		ExpressionNode parsed = ExpressionParser.parseExpression(expression);
		TypeCheckingContext context = new TypeCheckingContext();
		context.pushProcedureFrame();
		AnnotatedType type = checker.check(context, parsed);
		Assertions.assertEquals(PrimitiveType.Integer, type.getType());
		EvaluationContext evalContext = new EvaluationContext();
		evalContext.pushFrame();
		Value value = interpreter.compute(evalContext, parsed);
		Assertions.assertEquals(expected, value.getIntValue());
	}

	private void t(String expression, boolean expected) {
		ExpressionNode parsed = ExpressionParser.parseExpression(expression);
		TypeCheckingContext context = new TypeCheckingContext();
		context.pushProcedureFrame();
		AnnotatedType type = checker.check(context, parsed);
		Assertions.assertEquals(PrimitiveType.Boolean, type.getType());
		EvaluationContext evalContext = new EvaluationContext();
		evalContext.pushFrame();
		Value value = interpreter.compute(evalContext, parsed);
		Assertions.assertEquals(expected, value.getBooleanValue());
	}
}