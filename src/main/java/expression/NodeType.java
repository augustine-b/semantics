package expression;

public enum NodeType {

	Root {
		@Override
		public String toPrettyString(String[] childStrings) {
			return String.join("\n", childStrings);
		}
	},

	Integer,

	Boolean,

	Declaration,

	ProcedureDeclaration,

	ProcedureCall,

	Assignment("="),

	Identifier,

	If {
		@Override
		public String toPrettyString(String[] childStrings) {
			if (childStrings.length == 2) {
				return "if " + childStrings[0]
						+ " then " + childStrings[1];
			} else {
				return "if " + childStrings[0]
						+ " then " + childStrings[1]
						+ " else " + childStrings[2];
			}
		}
	},

	While {
		@Override
		public String toPrettyString(String[] childStrings) {
			return "while " + childStrings[0]
					+ " do " + childStrings[1];
		}
	},

	Block {
		@Override
		public String toPrettyString(String[] childStrings) {
			StringBuilder sb = new StringBuilder();
			sb.append("{\n");
			for (String childString : childStrings) {
				sb.append('\t').append(childString.replace("\n", "\n\t")).append(";\n");
			}
			sb.append("}");
			return sb.toString();
		}
	},

	Plus("+"),

	Minus("-"),

	Multiply("*"),

	Divide("/"),

	Modulo("%"),

	Equals("=="),

	NotEquals("!="),

	Less("<"),

	LessEquals("<="),

	Greater(">"),

	GreaterEquals(">="),

	And("&&"),

	Or("||"),

	Not("!");

	public final String code;

	NodeType() {
		this.code = null;
	}

	NodeType(String code) {
		this.code = code;
	}

	public static NodeType lookup(String code) {
		for (NodeType nodeType : values()) {
			if (code.equals(nodeType.code)) {
				return nodeType;
			}
		}

		throw new Error("Node type not found");
	}

	public String toPrettyString(String[] childStrings) {
		if (childStrings.length == 1) {
			return code + childStrings[0];
		}

		return "(" + childStrings[0] + " " + code + " " + childStrings[1] + ")";
	}
}
