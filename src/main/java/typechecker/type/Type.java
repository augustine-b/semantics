package typechecker.type;

import interpreter.value.Value;

public interface Type {
	String toPrettyString();

	// lhs can receive the value of rhs
	boolean supertypeOf(Type type);

	Value defaultValue();
}
