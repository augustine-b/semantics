package typechecker;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import typechecker.type.PrimitiveType;

public class TypeTest {

	@Test
	public void testSupertypeOf() {
		Assertions.assertTrue(PrimitiveType.Integer.supertypeOf(PrimitiveType.Integer));
		Assertions.assertFalse(PrimitiveType.Integer.supertypeOf(PrimitiveType.Boolean));
		Assertions.assertFalse(PrimitiveType.Integer.supertypeOf(PrimitiveType.Void));
		Assertions.assertFalse(PrimitiveType.Boolean.supertypeOf(PrimitiveType.Integer));
		Assertions.assertTrue(PrimitiveType.Boolean.supertypeOf(PrimitiveType.Boolean));
		Assertions.assertFalse(PrimitiveType.Boolean.supertypeOf(PrimitiveType.Void));
		Assertions.assertTrue(PrimitiveType.Void.supertypeOf(PrimitiveType.Integer));
		Assertions.assertTrue(PrimitiveType.Void.supertypeOf(PrimitiveType.Boolean));
		Assertions.assertTrue(PrimitiveType.Void.supertypeOf(PrimitiveType.Void));
	}
}
