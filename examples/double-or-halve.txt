proc doubleOrHalve(a: int, double: bool) {
	toExecute: <int> -> int;
	if (double) then {
		toExecute = proc(x: int) { 2 * x; };
	} else {
		toExecute = proc(x: int) { x / 2; };
	};
	toExecute(a);
}