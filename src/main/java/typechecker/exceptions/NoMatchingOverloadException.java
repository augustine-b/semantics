package typechecker.exceptions;

import typechecker.type.AnnotatedType;
import typechecker.type.CallableType;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class NoMatchingOverloadException extends RuntimeException {
	public NoMatchingOverloadException(String name, AnnotatedType[] childTypes, List<CallableType> procedures) {
		super("No matching overload found for procedure " + name
				+ " with parameters " + Arrays.toString(childTypes)
				+ "\nThe options are: " + procedures.stream().map(CallableType::toPrettyString)
					.collect(Collectors.joining(", ")));
	}
}
