package typechecker.type;

import typechecker.ValueCategory;

public class AnnotatedType {

	private final Type type;
	private final ValueCategory category;

	public AnnotatedType(Type type) {
		this(type, ValueCategory.RValue);
	}

	public AnnotatedType(Type type, ValueCategory category) {
		this.type = type;
		this.category = category;
	}

	public Type getType() {
		return type;
	}

	public ValueCategory getCategory() {
		return category;
	}

	@Override
	public String toString() {
		return "(" + type + ", " + category + ")";
	}

	public String toPrettyString() {
		return (category == ValueCategory.LValue ? "ref " : "") + type.toPrettyString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		AnnotatedType that = (AnnotatedType) o;
		return type.equals(that.type) && category == that.category;
	}

	public AnnotatedType asRValue() {
		return new AnnotatedType(type, ValueCategory.RValue);
	}

	public AnnotatedType asLValue() {
		return new AnnotatedType(type, ValueCategory.LValue);
	}

	// lhs can receive the value of the rhs
	public boolean supertypeOf(AnnotatedType other) {
		return (category == ValueCategory.RValue || other.category == ValueCategory.LValue)
				&& type.supertypeOf(other.type);
	}
}
