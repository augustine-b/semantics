package typechecker.type;

import interpreter.value.BooleanValue;
import interpreter.value.IntegerValue;
import interpreter.value.Value;
import interpreter.value.VoidValue;

public enum PrimitiveType implements Type {
	Integer("int") {
		@Override
		public Value defaultValue() {
			return new IntegerValue(0);
		}
	},

	Boolean("bool") {
		@Override
		public Value defaultValue() {
			return new BooleanValue(false);
		}
	},

	Void("void") {
		@Override
		public Value defaultValue() {
			return VoidValue.INSTANCE;
		}
	};

	private final String code;

	PrimitiveType(String code) {
		this.code = code;
	}

	@Override
	public String toPrettyString() {
		return code;
	}

	@Override
	public boolean supertypeOf(Type type) {
		return this == type || this == Void;
	}

	public static PrimitiveType lookup(String code) {
		for (PrimitiveType type : values()) {
			if (code.equals(type.code)) {
				return type;
			}
		}

		throw new Error("Primitive type not found");
	}
}
